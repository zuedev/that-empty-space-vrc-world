![](./ThatEmptySpace.Unity/thumbnail.png)

# That Empty Space

An empty‚ dark‚ and quiet space․

## Development

### Requirements

- Unity `v2022.3.6f1`
- VRChat SDK - Base `v3.5.0`
- VRChat SDK - Worlds `v3.5.0`
- VRChat Package Resolver Tool `v0.1.27`
