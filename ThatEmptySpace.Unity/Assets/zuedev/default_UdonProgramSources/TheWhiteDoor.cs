﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

namespace zuedev
{
    // TODO: add sound effect to door opening and closing
    public class TheWhiteDoor : UdonSharpBehaviour
    {
        private bool isActive;
        private float timer;
        private float timerMax = 5;

        // called when the scene starts
        void Start()
        {
            // hide the door
            gameObject.SetActive(false);
            isActive = false;
        }

        // called when a player joins
        public override void OnPlayerJoined(VRCPlayerApi player)
        {
            gameObject.SetActive(true);
            timer = 0;
            isActive = true;
        }

        private void Update()
        {
            if (isActive)
            {
                if (timer < timerMax)
                {
                    timer += Time.deltaTime;
                }
                else
                {
                    gameObject.SetActive(false);
                    isActive = false;
                }
            }
        }
    }
}

